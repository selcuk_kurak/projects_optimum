import React,{useEffect} from 'react'
import axios from 'axios'

const Anasayfa = () => {

    const kulllaniciGetir = () => {
        axios.get("/kullaniciBilgileri/")
            .then(response => console.log(response.data))
    }

    useEffect(() => {
        kulllaniciGetir()
    }, [])
    return(
        <h1>Kullanıcılar</h1>
    )
}

export default  Anasayfa