import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import { StoreContext } from 'storeon/react'
import store from "./store/girisKullanici"
import Anasayfa from "./anasayfa/Anasayfa";
import GirisSayfa from "./girisEkrani/GirisSayfa";
import IlanSayfa from "./Ilan/IlanSayfa";
import KayitSayfa from "./girisEkrani/KayitSayfa";

export default function App() {
  return (
      // <StoreContext.Provider value={store}>
        <Router>
            <Switch>
              <Route path="/anasayfa">
                <Anasayfa />
              </Route>
              <Route path="/giris">
                <GirisSayfa />
              </Route>
              <Route path="/ilanolustur">
                <IlanSayfa/>
              </Route>
              <Route path="/">
                <KayitSayfa />
              </Route>
            </Switch>
        </Router>
      // </StoreContext.Provider>
  );

}