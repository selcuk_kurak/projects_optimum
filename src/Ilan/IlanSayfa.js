import React,{useState} from 'react'
import {Container, Row, Col} from "react-grid-system";
import UstMenu from "../UstMenu";
import {FormGroup,Label,Input,Form, Button} from "reactstrap";
import styled from "styled-components";
import {Colors} from "@blueprintjs/core";
import {Arkaplan} from "../girisEkrani/GirisSayfa";

const OrtaDiv = styled.div`
    width:100%;
    height:600px;
    background-color:${Colors.LIGHT_GRAY1};
`

const IlanForm = styled(Form)`
    padding:1% 10%;
    width:100%;
    height:100%;
`
const IlanFormGrup = styled(FormGroup)`
    padding-top: 5%;
    height:30px;
    
`

const LabelYazi = styled(Label)`
    color:${Colors.DARK_GRAY3};
    font-weight:bold;
    font-size:1em;
    
`
const InputSelect = styled(Input)`
    margin-left:2%;
    width:30%;
    height:30px;
    
`
const InputText = styled(Input)`
    width:50%;
    height:100px;
`
const Buton = styled(Button)`
    background-color:${Colors.GREEN5};
    margin: 25% 70%;
    width:40%;
    height:30px;
    
   &:hover {
        cursor:pointer;
        background-color:${Colors.BLUE2};
   }
`
const Sektorler = [
    { value: 0, label: 'Bilişim' },
    { value: 1, label: 'İnşaat/Mimari' },
    { value: 2, label: 'Maliye' }
]
const Sehirler = [
    { value: 0, label:"İstanbul"},
    { value: 1, label:"Ankara"},
    { value: 2, label:"İzmir"},
    { value: 0, label:"Bursa"},
    { value: 0, label:"Konya"},
]
const IlanSayfa = props => {
    const [form, setForm] = useState([])

    const FormInputKaydet = (event) =>{
        setForm([
                {
                    ...form,
                    [event.target.name] : event.target.value
                }
            ]
        )
    }
    console.log(form)
    return(
        <Container fluid>
            <Row>
                <Col md={12}>
                    <UstMenu/>
                </Col>
            </Row>
            <Arkaplan>
                <Row>

                    <Col md={6}>

                        <IlanForm>
                            <IlanFormGrup>
                                <LabelYazi>Sektör Seçiniz:</LabelYazi>
                                <InputSelect type="select" name="sektor" onChange={FormInputKaydet} >
                                    {Sektorler.map(sektor => (
                                        <option>{sektor.label}</option>
                                    ))}
                                </InputSelect>
                            </IlanFormGrup>
                            <IlanFormGrup>
                                <LabelYazi>Meslek Seçiniz:</LabelYazi>
                                <InputSelect type="select" name="meslek" onChange={FormInputKaydet}>
                                    <option>Bilişim</option>
                                    <option>İnşaat/Mimari</option>
                                    <option>Maliye</option>
                                </InputSelect>
                            </IlanFormGrup>
                            <IlanFormGrup>
                                <LabelYazi>Şehir Seçiniz:</LabelYazi>
                                <InputSelect type="select" name="sehir" onChange={FormInputKaydet}>
                                    {Sehirler.map(sehir =>(
                                        <option>{sehir.label}</option>
                                    ))}
                                </InputSelect>
                            </IlanFormGrup>
                            <IlanFormGrup style={{marginTop:"10%"}}>
                                <LabelYazi>İlan Başlığı:</LabelYazi>
                                <InputSelect type="text" name="ilanBaslik" onChange={FormInputKaydet} />
                            </IlanFormGrup>
                            <IlanFormGrup>
                                <LabelYazi>İş Tanımı:</LabelYazi>
                                <InputText type="textarea" name="isTanimi" onChange={FormInputKaydet}/>
                            </IlanFormGrup>
                            <Buton>İlan Koşullarını Belirle</Buton>
                        </IlanForm>

                    </Col>
                    <Col md={6} style={{float:"left"}}>
                        <IlanForm >
                            <IlanFormGrup>
                                <LabelYazi style={{marginLeft:"19%"}}>Aranan Kişi Sayısı:</LabelYazi>
                                <InputSelect type="text" name="arananKisi" onChange={FormInputKaydet} />
                            </IlanFormGrup>
                            <IlanFormGrup>
                                <LabelYazi>Mülakata Çağırılacak Kişi Sayısı:</LabelYazi>
                                <InputSelect type="text" name="mulakatKisi" onChange={FormInputKaydet}/>
                            </IlanFormGrup>
                            <IlanFormGrup>
                                <LabelYazi style={{marginLeft:"21%"}} >Fotoğraf Yükleyin:</LabelYazi>
                                <InputSelect type="file" name="fotograf"  onChange={FormInputKaydet} />
                            </IlanFormGrup>
                        </IlanForm>
                    </Col>
                </Row>
            </Arkaplan>
        </Container>
    )
}







export default IlanSayfa