import React, {useState} from 'react';
import styled from "styled-components";
import {Alignment, Button, ButtonGroup, Colors, Navbar, NavbarDivider, NavbarGroup} from "@blueprintjs/core";
import {Link} from "react-router-dom";

const UstMenuDiv = styled.div`
    background-color:${Colors.TURQUOISE3};
    height:120px;
`
const UstMenuElemanlar = styled(Navbar)`
    padding:0% 20%
`
const Wrapper = styled.div`
    padding-bottom:-10px;
`
const ElemanButonlari = styled(Button)`
    min-width:15%;
    max-height:30px;
    margin-left:1%;
    background-color:${Colors.TURQUOISE3};
    border:none;
    position:initial;
    font-size:1.1em;
&:hover {
    background-color:${Colors.LIGHT_GRAY1};
    cursor:pointer;
 }
`
const Butonlar = styled(ButtonGroup)`
margin-left:85%;
padding-top:3%;
`
const Buton = styled(Button)`
    min-width:50%;
    height:40px;
    background-color:${props => props.text === "Giriş Yap" ? Colors.BLUE2 : Colors.FOREST1};
    border:none;
    border-radius:8px;
    
&:hover {
    background-color:${Colors.ORANGE3};
    cursor:pointer;
}
`
const SirketKullaniciIsmi = styled.div`
margin-left:85%;
padding-top:3%;
font-size:1.2em;
color:${Colors.DARK_GRAY5};
`
const UstMenu = props => {
    return(
        <UstMenuDiv>
            <Wrapper>
                <Butonlar>
                    <Link to="/giris">
                        <Buton text={"Giriş Yap"}/>
                    </Link>
                    <Link to={"/"}>
                        <Buton text={"Kaydol"}/>
                    </Link>

                </Butonlar>
                {props.sirketAktif && (
                    <SirketKullaniciIsmi>MZS BİLİŞİM</SirketKullaniciIsmi>
                )}
                {props.kullaniciAktif && (
                    <SirketKullaniciIsmi>SELÇUK KURAK</SirketKullaniciIsmi>
                )}
                <UstMenuElemanlar>
                    <NavbarGroup align={Alignment.CENTER}>
                        <NavbarDivider />
                        <Link to="/anasayfa">
                            <ElemanButonlari text="Anasayfa" />
                        </Link>
                        {(props.kullaniciAktif || props.kullaniciAktif === false) && props.sirketAktif === false && (
                            <ElemanButonlari text={"İş Arayın"}></ElemanButonlari>
                        )}
                        <ElemanButonlari text="Profilim" />
                        <ElemanButonlari text="İletişim" />
                    </NavbarGroup>
                </UstMenuElemanlar>
            </Wrapper>
        </UstMenuDiv>
    )
}

export default UstMenu