import React, {useState} from 'react'
import {Container, Row, Col} from "react-grid-system";
import styled from "styled-components";
import UstMenu from "../UstMenu";
import {ButtonGroup, Colors} from "@blueprintjs/core";
import {Icon} from "@blueprintjs/core";

import {ListGroup, ListGroupItem, Card, CardBody, CardTitle, CardSubtitle, CardText, Button} from  'reactstrap';
import {Arkaplan} from "../girisEkrani/GirisSayfa";
import {Link} from "react-router-dom";
import useStoreon from "storeon/react";

const YanMenu = styled.div`
width:100%;
min-height:100%;
background-color:${Colors.LIGHT_GRAY4};
`

const OrtaDiv = styled.div`
width:104%;
min-height:100%;
margin-left:-4%;
`

const KartBaslik = styled.div`
    font-size:1.4em;
    text-align:center;
    font-weight:bold;
    padding-top:3%;
    background-color:${Colors.LIGHT_GRAY3};
    color:${Colors.DARK_GRAY5}
`
const YanMenuElemanları = styled(ListGroupItem)`
    padding:15% 5%;
    list-style-type:none;
    font-size:1.1em;
`

const IlanButon = styled(Button)`
    width:10%;
    height:40px;
    margin:1% 90%;
    background-color:${Colors.ORANGE3};
    border:none;
    border-radius:5px;
    
   &:hover{
    cursor:pointer;
   }
`
const KartButon = styled(Button)`
    background-color:${props => (props.kullaniciAktif === true && props.sirketAktif ===false)  ? Colors.GREEN1 :Colors.BLUE2};
    border:none;
    border-radius:5px;
`
const IlanKart = styled(Card)`
    background-color:${Colors.GRAY1};
    margin:2% 10%;
    width:30%;
    float:left;
    text-align:center;
`
const Butonlar = styled(ButtonGroup)`
    padding:0% 25%;
    border:none;
`
const Buton = styled(Button)`
    width:50%;
    height:30px;
    border:none;
    cursor:pointer;
`
const ilanBasligi = [
    {id:0, baslik:"Yazılımcı Arıyoruz"},
    {id:1, baslik:"Sağlıkçı Arıyoruz"},
    {id:2, baslik:"Mühendis Arıyoruz"},
    {id:3, baslik:"Yazılımcı Arıyoruz"},
    {id:4, baslik:"Yazılımcı Arıyoruz"},
    {id:5, baslik:"Yazılımcı Arıyoruz"}
]
const GirisizAnasayfa = props => {
    const [sirketAktif, setSirketAktif] = useState(false)
    const [kullaniciAktif, setKullaniciAktif] = useState(false)
    // const girenKullanici = useStoreon('kullanici')

    // console.log(girenKullanici)
    const sirketButon = () => {
        setKullaniciAktif(false)
        setSirketAktif(true)
    }

    const kullaniciButon = () => {
        setKullaniciAktif(true)
        setSirketAktif(false)
    }
    console.log(props.girenKullanici)
    return(
        <Container fluid>
            <Row>
                <Col sm={12}>
                    <UstMenu sirketAktif={sirketAktif} kullaniciAktif={kullaniciAktif} sirketButon/>
                </Col>
            </Row>
            <Arkaplan>
                <Row>
                    <Col sm={4}>
                        <YanMenu>
                            <KartBaslik>
                                İŞ İLANLARI
                            </KartBaslik>
                            <Card>
                                <ListGroup>
                                    <YanMenuElemanları><Icon icon={"caret-right"} iconSize={"18px"}/>
                                        Sektöre Göre
                                    </YanMenuElemanları>
                                    <YanMenuElemanları><Icon icon={"caret-right"} iconSize={"18px"}/>Departmana Göre</YanMenuElemanları>
                                    <YanMenuElemanları><Icon icon={"caret-right"} iconSize={"18px"}/>Şehire Göre</YanMenuElemanları>
                                </ListGroup>
                            </Card>
                        </YanMenu>
                    </Col>
                    <Col sm={8}>
                        <OrtaDiv>
                            <Link to="ilanolustur">
                                <IlanButon>İlan Oluştur</IlanButon>
                            </Link>
                            <Butonlar>
                                <Buton style={{backgroundColor:sirketAktif === true ? Colors.BLUE2 : Colors.TURQUOISE3}} onClick={() => sirketButon()} >Şirket</Buton>

                                <Buton style={{backgroundColor:kullaniciAktif === true ? Colors.BLUE2 : Colors.TURQUOISE3}} onClick={() => kullaniciButon()} >Kullanıcı</Buton>
                            </Butonlar>
                            {ilanBasligi.map((baslik) => (
                                <IlanKart>
                                    {/*<CardImg top width="100%" src="/assets/318x180.svg" alt="Card image cap" />*/}
                                    <CardBody>
                                        <CardTitle style={{fontSize:"1.5em"}}>{baslik.baslik}</CardTitle>
                                        <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                                        {(sirketAktif === false || sirketAktif) && kullaniciAktif === false && (
                                            <KartButon kullaniciAktif={kullaniciAktif}>İlanı Görüntüle</KartButon>
                                        )}
                                        {sirketAktif === false && kullaniciAktif && (
                                            <Button kullaniciAktif={kullaniciAktif}>Başvuru Detayına Git</Button>
                                        )}
                                    </CardBody>
                                </IlanKart>
                            ))}
                        </OrtaDiv>
                    </Col>
                </Row>
            </Arkaplan>
        </Container>
    )
}
export default GirisizAnasayfa