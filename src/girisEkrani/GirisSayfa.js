import React,{useState, useEffect} from 'react'
import styled from "styled-components";
import {Container,Col,Row} from "react-grid-system";
import {Colors,ButtonGroup, Button,
    Switch} from "@blueprintjs/core";
import {Form, FormGroup, Input} from 'reactstrap'
import UstMenu from "../UstMenu";
import axios from 'axios';
import useStoreon from "storeon/react";
import {Link} from "react-router-dom";
import GirisizAnasayfa from "../anasayfa/GirissizAnasayfa";
const Wrapper = styled.div`
`
export const Arkaplan = styled.div`
    min-width:100%;
    min-height:100%;
    background: -moz-linear-gradient(#fff, #f8b500); /* FF3.6+ */
    background: -webkit-linear-gradient(#fff,#f8b500); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(#fff,#f8b500); /* Opera11.10+ */
    background: -ms-linear-gradient(#fff,#f8b500); /* IE10+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fff', endColorstr='#f8b500',GradientType=0 ); /* IE6-9 */
    background: linear-gradient(#fff,#f8b500); /* W3C */
`

export const GirisKismi = styled.div`
    background-color:${Colors.LIGHT_GRAY1};
    margin-top:20px;
    margin-left:40%;
    margin-right:20%;
    border-radius:10%;
    width:80%;
    height:500px;
`
export const Butonlar = styled(ButtonGroup)`
    padding:5% 35%;
    border:none;
`
export const Buton = styled(Button)`
    width:50%;
    height:30px;
    border:none;
    cursor:pointer;
`
const GirisButon = styled(Button)`
    width:20%;
    height:30px;
    border:none;
    margin:2% 40%;
    background-color:${Colors.FOREST3};
&:hover {
    background-color:${Colors.BLUE2};
    cursor:pointer;
}
`
export const InputYeri = styled.div`
    padding:0% 37%;
    margin-top:0%;
`
const Inputt = styled(Input)`
    margin-top:3%;
    margin-bottom:5%;
`
const Yazi = styled.div`
    margin:2% 35%;
    font-size:0.9em;
    font-style:italic;
    float:left;
`
const LinkYazi = styled.div`
 color:${Colors.BLUE2};
&:hover {
    text-decoration:underline;
    cursor:pointer;
 }
`


const GirisSayfa = props =>{
    const [sirketAktif, setSirketAktif] = useState(false)
    const [kullanici, setKullanici] = useState(null)
    const [kullaniciAktif, setKullaniciAktif] = useState(true)
    const [form, setForm] = useState({})
    const [girenKullanici, setGirenKullanici] = useState(null)
    // const [dispatch, girenKullanici] = useStoreon("kullanici")

    // console.log("global kullanıcı: ", girenKullanici )

    const formKaydet = (event) => {
        setForm({
            ...form,
                [event.target.name] : event.target.value
            }
        )
    }

    const girisYap = () => {
            const girisKullanici = kullanici.find(kullanici => kullanici.mail === form.email && kullanici.sifre === form.sifre)
            const id = girisKullanici.id
            girisKullanici.aktif = true

            axios.put("/kullaniciBilgileri/giris/" + id, girisKullanici)
                .then(response => setGirenKullanici(response.data))
    }

    console.log("girenKullanici", girenKullanici)
    const sirketButon = () => {
        setKullaniciAktif(false)
        setSirketAktif(true)
    }

    const kullaniciButon = () => {
        setKullaniciAktif(true)
        setSirketAktif(false)
    }
    const kullaniciGetir = () => {
        axios.get('kullaniciBilgileri/')
            .then(response => setKullanici(response.data))
    }
    useEffect(() => {
        kullaniciGetir()
    }, [])
    console.log("kullanici", kullanici)
    console.log(form)
    return (
        <Wrapper>
            <Container fluid>
                <Row>
                    <Col md={12}>
                        <UstMenu/>
                    </Col>
                </Row>
                <Row>
                    <Arkaplan>
                        <Col md={2}></Col>
                        <Col md={8}>
                            <GirisKismi>
                                <Butonlar>
                                    <Buton style={{backgroundColor:sirketAktif === true ? Colors.BLUE2 : Colors.TURQUOISE3}} onClick={() => sirketButon()} text={"Şirket"}/>

                                    <Buton style={{backgroundColor:kullaniciAktif === true ? Colors.BLUE2 : Colors.TURQUOISE3}} onClick={() => kullaniciButon()} text={"Kullanıcı"}></Buton>
                                </Butonlar>
                                <InputYeri>
                                    <Form>
                                        <FormGroup>
                                            {sirketAktif && (
                                                <Inputt type="email" name={"sirketEmail"} style={{width:"95%", height:"20px"}} placeholder={sirketAktif === true ? "Şirket E-Mailini Giriniz" : "E-Mailinizi Girinizi"}  onChange={formKaydet}  />
                                            )}
                                            {kullaniciAktif && (
                                                <Inputt type="email" name={"email"} style={{width:"95%", height:"20px"}} placeholder={sirketAktif === true ? "Şirket E-Mailini Giriniz" : "E-Mailinizi Girinizi"}  onChange={formKaydet}  />
                                            )}
                                        </FormGroup>
                                        <FormGroup>
                                            {sirketAktif && (
                                                <Inputt type="password" name={"sirketSifre"}   style={{width:"95%", height:"20px"}} placeholder="Şifreyi Giriniz..." onChange={formKaydet} />
                                            )}
                                            {kullaniciAktif && (
                                                <Inputt type="password" name={"sifre"}   style={{width:"95%", height:"20px"}} placeholder="Şifreyi Giriniz..."  onChange={formKaydet}/>
                                            )}
                                        </FormGroup>
                                    </Form>

                                </InputYeri>
                                <Switch style={{marginLeft:"50%"}} labelElement={<em>Beni Hatırla</em>} />
                                <Link to={"/anasayfa"}>
                                    <GirisButon text={"Giriş Yap"} onClick={girisYap}/>
                                </Link>

                                <Yazi>
                                    Üye değilseniz lütfen
                                    <LinkYazi>kayıt olunuz.</LinkYazi>
                                </Yazi>
                            </GirisKismi>
                        </Col>
                        <Col md={2}>
                        </Col>
                    </Arkaplan>
                </Row>
            </Container>
            {girenKullanici && (
                <GirisizAnasayfa kullanici={girenKullanici}/>
            )}
        </Wrapper>
    )


}
export default GirisSayfa;