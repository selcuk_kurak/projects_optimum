import React, {useState} from 'react';
import UstMenu from "../UstMenu";
import {Container,Row,Col} from "react-grid-system";
import {Arkaplan, Buton, Butonlar, GirisKismi, InputYeri} from "./GirisSayfa";
import {Button, Colors,Alert} from "@blueprintjs/core";
import {Form, FormGroup, Label, Input} from "reactstrap";
import styled from "styled-components";
import axios from "axios"
import {Link} from "react-router-dom";
import girisKullanici from "../store/girisKullanici";
import useStoreon from "storeon/react";

const KayitInput = styled(Input)`
    margin-top:3%;
    margin-bottom:5%;
`
const KayitButon = styled(Button)`
     width:30%;
    height:30px;
    border:none;
    margin:2% 30%;
    background-color:${Colors.FOREST3};
&:hover {
    background-color:${Colors.BLUE2};
    cursor:pointer;
}
`
const Yazi = styled.div`
    margin:2% 10%;
    font-size:0.9em;
    font-style:italic;
    float:left;
`
const LinkYazi = styled.div`
 color:${Colors.BLUE2};
&:hover {
    text-decoration:underline;
    cursor:pointer;
 }
`
const KayitSayfa = props => {
    const [sirketAktif, setSirketAktif] = useState(false)
    const [kullaniciAktif, setKullaniciAktif] = useState(true)
    const [form, setForm] = useState({})
    const [alert, setAlert] = useState(false);

    const sirketButon = () => {
        setKullaniciAktif(false)
        setSirketAktif(true)
    }
    console.log(form)
    const kullaniciKaydet = () => {
        if(form.sifre === form.sifreTekrar){
            const kullanici = {
                ad : form.ad,
                soyad : form.soyad,
                mail : form.mail,
                sifre : form.sifre,
                cinsiyet: false,
                ehliyetDurumu: false,
                yuksekLisansDurumu: false,
                aktif: false,
                dogumTarihi: "1960-01-01T21:00:00.000+0000",
                askerlikDurumu: null,
                sehir: null,
                telefon: null,
                liseAd: null,
                liseMezunTarihi: "1975-01-01T21:00:00.000+0000",
                uniAd: null,
                fakulteAd:null,
                uniMezunTarihi:"1980-01-01T21:00:00.000+0000",
                doktoraDurumu:false,
                dilBilgisi:null,
                meslek:null,
                deneyim:null,
                sektor:null,
                departman:null,
                unvan:null,
                fotograf:null

            }
            axios.post("kullaniciBilgileri/kullaniciKayit", kullanici)
                alert("Başarılı bir şekilde kayıt oldunuz. Lütfen Giriş Yapınız.")
        }

        else
            alert("Şifreler uyuşmuyor, Tekrar Deneyiniz")

    }
    const formKaydet = (event) => {
            setForm({
                    ...form,
                    [event.target.name] : event.target.value
                }
            )
    }
    const kullaniciButon = () => {
        setKullaniciAktif(true)
        setSirketAktif(false)
    }
    return(
        <Container fluid>
            <Row>
                <Col md={12}>
                    <UstMenu/>
                </Col>
            </Row>
            <Row>
                <Arkaplan>
                    <Col md={2}>

                    </Col>
                    <Col md={8}>
                        <GirisKismi>
                            <Butonlar>
                                <Buton style={{backgroundColor:sirketAktif === true ? Colors.BLUE2 : Colors.TURQUOISE3}} text={"Şirket"} onClick={() => sirketButon()}></Buton>
                                <Buton style={{backgroundColor:kullaniciAktif === true ? Colors.BLUE2 : Colors.TURQUOISE3}} text={"Kullanıcı"} onClick={() => kullaniciButon()}></Buton>
                            </Butonlar>
                            <InputYeri>
                                {kullaniciAktif && (
                                    <Form>
                                        <FormGroup>
                                            <Label>Adı</Label>
                                            <KayitInput type="text" name="ad"  placeholder="İsminizi Giriniz..."  onChange={formKaydet}/>
                                        </FormGroup>
                                        <FormGroup>
                                            <Label>Soyadı</Label>
                                            <KayitInput type="text" name="soyad"  placeholder="Soyisminizi Giriniz..." onChange={formKaydet}/>
                                        </FormGroup>
                                        <FormGroup>
                                            <Label>E-Mail</Label>
                                            <KayitInput type="email" name="mail"  placeholder="Mail Adresinizi Giriniz..." onChange={formKaydet}/>
                                        </FormGroup>
                                        <FormGroup>
                                            <Label>Şifre</Label>
                                            <KayitInput type="password" name="sifre"  placeholder="Şifrenizi Giriniz" onChange={formKaydet}/>
                                        </FormGroup>
                                        <FormGroup>
                                            <Label>Şifre Tekrarı</Label>
                                            <KayitInput type="password" name="sifreTekrar"  placeholder="Şifrenizi Onaylayınız..." onChange={formKaydet}/>
                                        </FormGroup>
                                        <KayitButon onClick={kullaniciKaydet}>
                                            Kaydol
                                        </KayitButon>


                                    </Form>
                                )}
                                {sirketAktif && (
                                    <Form>
                                        <FormGroup>
                                            <Label>Şirket Adı</Label>
                                            <KayitInput type="text" name="sirketAd"  placeholder="Şirket Adını Giriniz..." onChange={formKaydet}/>
                                        </FormGroup>
                                        <FormGroup>
                                            <Label>E-Mail</Label>
                                            <KayitInput type="email" name="sirketMail"  placeholder="E-mail Adresini Giriniz..." onChange={formKaydet}/>
                                        </FormGroup>
                                        <FormGroup>
                                            <Label>Şifre</Label>
                                            <KayitInput type="password" name="sirketSifre"  placeholder="Şifrenizi Giriniz" onChange={formKaydet}/>
                                        </FormGroup>
                                        <FormGroup>
                                            <Label>Şifre Tekrarı</Label>
                                            <KayitInput type="password" name="sirketSifreTekrar"  placeholder="Şifrenizi Onaylayınız..." onChange={formKaydet}/>
                                        </FormGroup>
                                        <KayitButon>Kaydol</KayitButon>
                                    </Form>
                                )}
                                <Yazi>
                                    Üye iseniz lütfen
                                    <LinkYazi>giriş yapınız.</LinkYazi>
                                </Yazi>
                            </InputYeri>
                        </GirisKismi>
                    </Col>

                </Arkaplan>
            </Row>
        </Container>
    )
}
export default KayitSayfa