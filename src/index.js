import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import store from "./store/girisKullanici";
import StoreContext from "storeon/react";
ReactDOM.render(
    // <StoreContext.Provider store={store}>
        <App />,
    // </StoreContext.Provider>
  document.getElementById('root')
);
