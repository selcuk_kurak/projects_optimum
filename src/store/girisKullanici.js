import createStore from 'storeon'
const kullanici = {
    ad:'',
    soyad:'',
    mail:'',
    sifre:'',
    aktif:false
}


const girisKullanici  = store => {
    store.on('@init', () => ( {kullanici} ) )

    store.on('kullanici/giris', ({kullanici}, giris) => {
        return {...kullanici,
            ad: giris.ad,
            soyad: giris.soyad,
            mail: giris.mail,
            sifre: giris.sifre,
            aktif: giris.aktif
        }
    })
}
const store = createStore([girisKullanici])
export default store